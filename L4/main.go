package main

import (
	"image"
	"os"
	"time"

	_ "image/png"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

type gameObj struct {
	Ext    float64
	Speed  float64
	Pos    pixel.Vec
	Mat    pixel.Matrix
	Sprite pixel.Sprite
}

type background struct {
	gameObj
}

type ship struct {
	gameObj
	Sprite [2][]*pixel.Sprite
	Line   int
	Img    int
	Timer  float64
}

func (s *ship) Control(win *pixelgl.Window, dt float64) {
	if win.Pressed(pixelgl.KeyLeft) {
		s.Pos.X -= s.Speed * dt
		if s.Timer == 10 && s.Img > 0 {
			s.Img--
		}
	}
	if win.Pressed(pixelgl.KeyRight) {
		s.Pos.X += s.Speed * dt
		if s.Timer == 10 && s.Img < 4 {
			s.Img++
		}
	}
	if !win.Pressed(pixelgl.KeyLeft) && !win.Pressed(pixelgl.KeyRight) {
		s.Img = 2
	}
	if win.Pressed(pixelgl.KeyUp) {
		s.Pos.Y += s.Speed * dt
	}
	if win.Pressed(pixelgl.KeyDown) {
		s.Pos.Y -= s.Speed * dt
	}

	if s.Pos.X-s.Sprite[s.Line][s.Img].Frame().W() <= 0 {
		s.Pos.X = 0 + s.Sprite[s.Line][s.Img].Frame().W()
	}
	if s.Pos.X+s.Sprite[s.Line][s.Img].Frame().W() >= 800 {
		s.Pos.X = 800 - s.Sprite[s.Line][s.Img].Frame().W()
	}
	if s.Pos.Y-s.Sprite[s.Line][s.Img].Frame().H() <= 0 {
		s.Pos.Y = 0 + s.Sprite[s.Line][s.Img].Frame().H()
	}
	if s.Pos.Y+s.Sprite[s.Line][s.Img].Frame().H() >= 600 {
		s.Pos.Y = 600 - s.Sprite[s.Line][s.Img].Frame().H()
	}
}

func (s *ship) Update(dt float64) {
	if s.Timer > 0 {
		s.Timer -= 20 * dt
	} else {
		s.Timer = 10
		if s.Line < 1 {
			s.Line++
		} else {
			s.Line = 0
		}
	}
}

func (s *ship) Draw(win *pixelgl.Window) {
	s.Sprite[s.Line][s.Img].Draw(win, s.Mat)
}

func loadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Awesome Game",
		Bounds: pixel.R(0, 0, 800, 600),
		VSync:  false,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	pic, err := loadPicture("IMG/ship.png")
	if err != nil {
		panic(err)
	}
	shp := ship{
		gameObj: gameObj{
			Ext:   2,
			Pos:   pixel.V(win.Bounds().W()/2, 50),
			Speed: 180.0,
		},
		Line:  0,
		Img:   2,
		Timer: 10,
	}
	y := pic.Bounds().Min.Y
	for x := pic.Bounds().Min.X; x < pic.Bounds().Max.X; x += 16 {
		shp.Sprite[0] = append(shp.Sprite[0], pixel.NewSprite(pic,
			pixel.R(x, y, x+16, y+24)))
	}
	y += 24
	for x := pic.Bounds().Min.X; x < pic.Bounds().Max.X; x += 16 {
		shp.Sprite[1] = append(shp.Sprite[1], pixel.NewSprite(pic,
			pixel.R(x, y, x+16, y+24)))
	}

	pic, err = loadPicture("IMG/desert-backgorund-looped.png")
	if err != nil {
		panic(err)
	}
	bg := background{
		gameObj: gameObj{
			Ext:    3.2,
			Pos:    pixel.V(400, 970),
			Speed:  60.0,
			Sprite: *pixel.NewSprite(pic, pic.Bounds()),
		},
	}

	last := time.Now()
	for !win.Closed() {
		dt := time.Since(last).Seconds()
		last = time.Now()

		bg.Pos.Y -= bg.Speed * dt
		if bg.Pos.Y <= 0 {
			bg.Pos.Y = 970
		}
		bg.Mat = pixel.IM.Scaled(pixel.ZV, bg.Ext).Moved(bg.Pos)
		bg.Sprite.Draw(win, bg.Mat)

		shp.Control(win, dt)
		shp.Mat = pixel.IM.Scaled(pixel.ZV, shp.Ext).Moved(shp.Pos)
		shp.Update(dt)
		shp.Draw(win)

		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
