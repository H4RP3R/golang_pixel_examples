package main

import (
	"image"
	"os"

	_ "image/png"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

type background struct {
	Ext    float64
	Speed  float64
	Pos    pixel.Vec
	Mat    pixel.Matrix
	Sprite pixel.Sprite
}

func loadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Awesome Game",
		Bounds: pixel.R(0, 0, 800, 600),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	pic, err := loadPicture("IMG/ship.png")
	if err != nil {
		panic(err)
	}
	sprite := pixel.NewSprite(pic, pixel.R(32, 0, 48, 24))

	pic, err = loadPicture("IMG/desert-backgorund-looped.png")
	if err != nil {
		panic(err)
	}
	bg := background{
		Ext:    3.2,
		Pos:    pixel.V(400, 970),
		Speed:  1.0,
		Sprite: *pixel.NewSprite(pic, pic.Bounds()),
	}

	for !win.Closed() {
		win.Clear(colornames.Midnightblue)

		bg.Pos.Y -= bg.Speed
		if bg.Pos.Y <= 0 {
			bg.Pos.Y = 970
		}
		bg.Mat = pixel.IM.Scaled(pixel.ZV, bg.Ext).Moved(bg.Pos)
		bg.Sprite.Draw(win, bg.Mat)

		sprite.Draw(win, pixel.IM.Scaled(pixel.ZV, 2).Moved(
			pixel.V(win.Bounds().W()/2, 50)))

		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
