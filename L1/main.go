package main

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Awesome Game",
		Bounds: pixel.R(0, 0, 800, 600),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	bgColor := pixel.RGB(0.6, 0, 0.8)

	for !win.Closed() {
		win.Clear(bgColor)

		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
