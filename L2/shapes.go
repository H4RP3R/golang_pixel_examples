package main

import (
	"math"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

func run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Awesome Game",
		Bounds: pixel.R(0, 0, 800, 600),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	imd := imdraw.New(nil)

	// круг
	imd.Color = colornames.Plum
	imd.Push(pixel.V(100, 500))
	imd.Circle(50, 0)
	// эллипс
	imd.Color = colornames.Dodgerblue
	imd.Push(pixel.V(300, 500))
	imd.Ellipse(pixel.V(75, 50), 4)
	// прямоугольник
	imd.Color = colornames.Limegreen
	imd.Push(pixel.V(440, 440), pixel.V(540, 540))
	imd.Rectangle(0)
	// дуги
	imd.Color = colornames.Crimson
	imd.EndShape = imdraw.RoundEndShape
	imd.Push(pixel.V(100, 350))
	imd.CircleArc(50, 0, 0.5*math.Pi, 10)
	imd.Push(pixel.V(300, 350))
	imd.CircleArc(50, -0.5*math.Pi, 0.5*math.Pi, 10)
	imd.Push(pixel.V(500, 350))
	imd.CircleArc(50, -1*math.Pi, 0.5*math.Pi, 10)
	// можно передать сразу несколько векторов
	// фигуры будут построены по каждой из переданных координат
	imd.Color = colornames.Navy
	imd.Push(pixel.V(100, 180), pixel.V(300, 180), pixel.V(500, 180))
	imd.EllipseArc(pixel.V(80, 40), 0, 1.7*math.Pi, 0)
	// линия по заданным точкам
	imd.Color = colornames.Lightgreen
	imd.EndShape = imdraw.SharpEndShape
	imd.Push(pixel.V(100, 30), pixel.V(250, 130),
		pixel.V(500, 30), pixel.V(750, 230))
	imd.Line(12)

	for !win.Closed() {
		win.Clear(colornames.Snow)
		imd.Draw(win)
		win.Update()
	}
}

func main() {
	pixelgl.Run(run)
}
